package school.com.teachersclient.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import school.com.teachersclient.Adapter.EventsAdapter;
import school.com.teachersclient.Pojo.EventsPojo;
import school.com.teachersclient.R;

public class EventsActivity extends Activity {
    private RecyclerView recycle;
    private ArrayList<EventsPojo> eventList=new ArrayList<>();
    private EventsAdapter mAdapter;
    private ImageView back;
    private ImageView noti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        recycle=(RecyclerView)findViewById(R.id.recycle_events);
        noti=findViewById(R.id.noti);


        eventList=new ArrayList<>();
        AnalData();

        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new EventsAdapter(eventList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());

        recycle.setAdapter(mAdapter);

        mAdapter.setOnClickListen(new EventsAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getApplicationContext(), AddEventActivity.class);
                startActivity(intent);

            }
        });

    }
    private void AnalData() {

        EventsPojo s=new EventsPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("Harvest Fest ");
        s.setContent("added 12 min ago");
        s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
        s.setDate("15 Sep 2018");
        s.setTime("2.30Pm");



        eventList.add(s);


        s=new EventsPojo();
        s.setTitle("Arts Celebration");
        s.setImg(String.valueOf(R.drawable.user));
        s.setContent("added 10 min ago");
        s.setDesc("March is Youth Art Month,the deal time to turn your's schools spotlight on arts.They do that");
        s.setDate("20 Sep 2018");
        s.setTime("5.30Pm");

        eventList.add(s);


        s=new EventsPojo();
        s.setTitle("Sports Celebration");
        s.setContent("added 3 min ago");
        s.setImg(String.valueOf(R.drawable.user));
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        s.setTime("3.30Pm");
        s.setDate("15 Sep 2018");
        eventList.add(s);


        s=new EventsPojo();
        s.setTitle("Talentia Competition");
        s.setContent("added 5 min ago");
        s.setDesc("The Speech competition is organising by school for all students");
        s.setTime("10.30Am");
        s.setDate("13 Sep 2018");
        s.setImg(String.valueOf(R.drawable.user));
        eventList.add(s);


    }

}
