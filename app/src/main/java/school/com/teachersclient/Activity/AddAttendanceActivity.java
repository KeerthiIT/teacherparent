package school.com.teachersclient.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.AddAttenAdapter;
import school.com.teachersclient.Pojo.AddAttenpojo;
import school.com.teachersclient.R;

public class AddAttendanceActivity extends Activity {

    private Spinner class_spinner;
    private RecyclerView recyclerView;
    private TextView present;
    private AddAttenAdapter mAdapter,adapter;
    private List<AddAttenpojo> addattenList=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_attendance);

        class_spinner=(Spinner)findViewById(R.id.class_spinner);
        recyclerView=(RecyclerView)findViewById(R.id.add_attendance_recycle);

        mAdapter = new AddAttenAdapter(addattenList);


        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        AttenData();
        present=(TextView)findViewById(R.id.present);

        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Attendance Capture Successfully",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),NewMessageActivity.class);
                startActivity(intent);
            }
        });

        List<String> categories = new ArrayList<String>();
        categories.add("Class 5A");
        categories.add("Class 5B");
        categories.add("Class 5C");
        categories.add("Class 6A");
        categories.add("Class 6B");
        categories.add("Class 6C");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, categories);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });







    }

    private void AttenData() {

    AddAttenpojo s=new AddAttenpojo();
    s.setImg(String.valueOf(R.drawable.dp));
    s.setName("Alexander");
    addattenList.add(s);

        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Allan");
        addattenList.add(s);


        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setName("BristoBrak");
        addattenList.add(s);

        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.usergirl));
        s.setName("CharlesMick");
        addattenList.add(s);

        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setName("David Cooper");
        addattenList.add(s);

        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.usergirl));
        s.setName("DavCone");
        addattenList.add(s);

        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Daniel");
        addattenList.add(s);
        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setName("Dansi");
        addattenList.add(s);

        s=new AddAttenpojo();
        s.setImg(String.valueOf(R.drawable.usergirl));
        s.setName("Elessa");
        addattenList.add(s);

    }
}
