package school.com.teachersclient.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;

import school.com.teachersclient.Adapter.RecycleViewAdapter;
import school.com.teachersclient.Pojo.GridPojo;
import school.com.teachersclient.R;

public class GridviewActivity extends Activity implements RecycleViewAdapter.ItemListener{

    RecyclerView recyclerView;
    ArrayList arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridview);


        recyclerView = (RecyclerView) findViewById(R.id.recycle);
        arrayList = new ArrayList();
        arrayList.add(new GridPojo("R.drawable.dp"));
        arrayList.add(new GridPojo("R.drawable.dp"));
        arrayList.add(new GridPojo("R.drawable.dp"));

        RecycleViewAdapter adapter = new RecycleViewAdapter(this, arrayList, this);
        recyclerView.setAdapter(adapter);

        /**
         AutoFitGridLayoutManager that auto fits the cells by the column width defined.
         **/

        /*AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 500);
        recyclerView.setLayoutManager(layoutManager);*/


        /**
         Simple GridLayoutManager that spans two columns
         **/
        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
    }


    @Override
    public void onItemClick(GridPojo item) {
        Toast.makeText(getApplicationContext(), item.dp + " is clicked", Toast.LENGTH_SHORT).show();

    }
}
