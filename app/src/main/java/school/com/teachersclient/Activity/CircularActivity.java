package school.com.teachersclient.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

import school.com.teachersclient.Adapter.CircularAdapter;
import school.com.teachersclient.Pojo.CircularPojo;
import school.com.teachersclient.R;

public class CircularActivity extends Activity {

    private RecyclerView recycle;
    private ArrayList<CircularPojo> circularlist=new ArrayList<>();
    private CircularAdapter mAdapter;
    private ImageView back;
    private ImageView menuu;
    private View v;
    private Context context;
    private PopupWindow pwindo;
    private int mViewSpacingLeft;
    private int mViewSpacingTop;
    private int mViewSpacingRight;
    private int mViewSpacingBottom;
    private boolean mViewSpacingSpecified = false;
    private TextView desc;
    private static final int MAX_LINES =2;
    private TextView mResizableTextView;
    private String text = "Welcome to Android Master, This is online tutorials for Android Development from beginners and all.We offer collections of  different Android tutorials which are commonly used in development now days.";
    private ImageView noti;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circular);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        noti=findViewById(R.id.noti);

        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });
        recycle=(RecyclerView)findViewById(R.id.recycle_circular);
        circularlist=new ArrayList<>();
        AnalData();

        back=(ImageView)findViewById(R.id.back);
        //  menuu=(ImageView)findViewById(R.id.menu);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new CircularAdapter(circularlist,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());

        recycle.setAdapter(mAdapter);

        mAdapter.setOnClickListen(new CircularAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {



                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.custom_layout, null);
                final TextView edit = alertLayout.findViewById(R.id.edit);
                final TextView delete = alertLayout.findViewById(R.id.delete);



                AlertDialog.Builder alert = new AlertDialog.Builder(new ContextThemeWrapper(CircularActivity.this, R.style.actionSheetTheme1));
                //   AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext(),getApplication().Resource.Style.MessageDialog);
                //   alert.setTitle("Info");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(false);

                AlertDialog dialog = alert.create();
                dialog.show();
                dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
            }
        });

        mAdapter.setOnClickListen(new CircularAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Intent intent=new Intent(getApplicationContext(),AddCircularActivity.class);
                startActivity(intent);

            }
        });


    }


    private void AnalData() {

        CircularPojo s=new CircularPojo();
        s.setTitle("Field Trip for Class II");
        s.setContent("14 Sep 2018 at 10Am");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");



        circularlist.add(s);


        s=new CircularPojo();
        s.setTitle("Working day on 2nd Saturaday");
        s.setContent("14 Sep 2018 at 5Pm");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");

        circularlist.add(s);


        s=new CircularPojo();
        s.setTitle("Tennis coaching in Before and After School hours for Classes X to XII");
        s.setContent("11 Sep at 5Am");

        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");
        circularlist.add(s);


        s=new CircularPojo();
        s.setTitle("Talentia Competition for VI to IX");
        s.setContent("21 Sep 2018 at Afternoon");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");

        circularlist.add(s);


        s=new CircularPojo();
        s.setTitle("Quiz Competion for XII");
        s.setContent("10 Sep at 2018");
        s.setDesc("The school is organising an educational field trip to Traffic Park.Goshamahal for Students of class");

        circularlist.add(s);
    }
}


