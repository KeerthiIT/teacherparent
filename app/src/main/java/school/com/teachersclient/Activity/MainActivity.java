package school.com.teachersclient.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.LeaveAdapter;
import school.com.teachersclient.Pojo.LeavePojo;
import school.com.teachersclient.R;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerview;
    private LeaveAdapter mAdapter;
    ImageView back;
    private List<LeavePojo> leaveList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerview=(RecyclerView)findViewById(R.id.recycle);
        leaveList=new ArrayList<>();
        AnalData();

        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //  recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new LeaveAdapter(leaveList,MainActivity.this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerview.setLayoutManager(mLayoutManager);

        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.setAdapter(mAdapter);

        mAdapter.setOnClickListen(new LeaveAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                 /*   Intent intent=new Intent(getApplicationContext(),ExamsActivity.class);
                startActivity(intent);*/
                Intent intent=new Intent(getApplicationContext(),EventsActivity.class);
                startActivity(intent);
            }
        });


    }

        private void AnalData() {



            LeavePojo s=new LeavePojo();
            s.setImg(String.valueOf(R.drawable.user));
            s.setTitle("Alexander");
            s.setSubtitle("VII");
            s.setDate("21/007/2018");
            s.setDesc("Absent");
            s.setTime("12 minutes ago");
            s.setSubdesc("Suffering from fever");
            leaveList.add(s);


            s=new LeavePojo();
            s.setImg(String.valueOf(R.drawable.user));
            s.setTitle("Rahul");
            s.setSubtitle("VII");
            s.setDate("21/007/2018");
            s.setDesc("Absent");
            s.setTime("2 minutes ago");
            s.setSubdesc("Suffering from fever");
            leaveList.add(s);


            s=new LeavePojo();
            s.setImg(String.valueOf(R.drawable.user));
            s.setTitle("Pradeep");
            s.setSubtitle("VII");
            s.setTime("2 minutes ago");
            s.setDate("21/007/2018");
            s.setDesc("Absent");
            s.setSubdesc("Suffering from fever");
            leaveList.add(s);


            s=new LeavePojo();
            s.setImg(String.valueOf(R.drawable.user));
            s.setTitle("Sandeep");
            s.setSubtitle("VII");
            s.setDate("21/007/2018");
            s.setDesc("Absent");
            s.setTime("10 minutes ago");
            s.setSubdesc("Suffering from fever");
            leaveList.add(s);

        }
}
