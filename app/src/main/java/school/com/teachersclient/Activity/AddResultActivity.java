package school.com.teachersclient.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.AddResultAdapter;
import school.com.teachersclient.Pojo.AddResultPojo;
import school.com.teachersclient.R;

public class AddResultActivity extends Activity {

    private Spinner class_spinner,sub_spinner,exam_spinner;
    private RecyclerView recyclerview;
    private AddResultAdapter mAdapter;
    private List<AddResultPojo> addresList=new ArrayList<>();
    private Button result_btn;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_result);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        class_spinner=(Spinner)findViewById(R.id.class_spinner);
        sub_spinner=(Spinner)findViewById(R.id.sub_spinner);
        exam_spinner=(Spinner)findViewById(R.id.exam_spinner);
        recyclerview=(RecyclerView)findViewById(R.id.recycle_add_result);
        result_btn=(Button)findViewById(R.id.result_btn);


        mAdapter = new AddResultAdapter(addresList,this);

        AddResultData();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recyclerview.setLayoutManager(mLayoutManager);

        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.setAdapter(mAdapter);


        result_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Added Successfully",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),ClassworkActivity.class);
                startActivity(intent);
            }
        });

        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Class 5C");
        classpinner.add("Class 4C");
        classpinner.add("Class 6C");
        classpinner.add("Class 7A");
        classpinner.add("Class 8B");
        classpinner.add("Class 9C");
        class_spinner = (Spinner) findViewById(R.id.class_spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        //subject spinner
        List<String> subspinner = new ArrayList<String>();
        subspinner.add("English");
        subspinner.add("Tamil");
        subspinner.add("Maths");
        subspinner.add("Science");
        subspinner.add("Social");
        subspinner.add("Computer Science");
        sub_spinner = (Spinner) findViewById(R.id.sub_spinner);

        ArrayAdapter<String> subspinnerArrayadapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, subspinner);

        subspinnerArrayadapter.setDropDownViewResource(R.layout.spinner_item);

        sub_spinner.setAdapter(subspinnerArrayadapter);

        sub_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        List<String> exam = new ArrayList<String>();
        exam.add("Ist Term Exam");
        exam.add("IInd Mid Term Exam");
        exam.add("Quartrely Exam");
        exam.add("Half Yearly Exam");

        exam_spinner = (Spinner) findViewById(R.id.exam_spinner);

        ArrayAdapter<String> examspinneradapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, exam);

        examspinneradapter.setDropDownViewResource(R.layout.spinner_item);

        exam_spinner.setAdapter(examspinneradapter);

        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
    }

    private void AddResultData() {
        AddResultPojo s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Jaun Clevenston");
        addresList.add(s);


        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setName("Einstein");
        addresList.add(s);

        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.usergirl));
        s.setName("Arnorld");
        addresList.add(s);

        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("James");
        addresList.add(s);

        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setName("John");
        addresList.add(s);

        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.usergirl));
        s.setName("Clinton");
        addresList.add(s);

        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setName("Robert Clive");
        addresList.add(s);

        s=new AddResultPojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setName("Lord Williams");
        addresList.add(s);

    }
}
