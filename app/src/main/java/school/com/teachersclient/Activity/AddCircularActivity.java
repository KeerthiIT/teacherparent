package school.com.teachersclient.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import school.com.teachersclient.R;

public class AddCircularActivity extends Activity {

    private EditText edit, editdesc;
    private Spinner spinner, spinner_class;
    private LinearLayout upload_linear;
    private RadioButton radio;
    private RadioGroup radiogroup;
    private String radioButton = "1";
    private ImageView img_upload;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    private int GALLERY=1;
    private Button add_cir_btn;

    private static final String IMAGE_DIRECTORY = "/demonuts";
    private CheckBox check;
    private ImageView back,backarrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_circular);



        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



       check=(CheckBox)findViewById(R.id.check);
        img_upload=(ImageView)findViewById(R.id.img_upload);
        add_cir_btn=(Button)findViewById(R.id.add_cir_btn);


        edit = (EditText) findViewById(R.id.edit_title);

        editdesc = (EditText) findViewById(R.id.edit_desc);

        upload_linear = (LinearLayout) findViewById(R.id.upload_linear);



        add_cir_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),ResultActivity.class);
                startActivity(intent);
            }
        });
     //check box on click
     check.setOnClickListener(new View.OnClickListener() {
         public int position;
         public int selectedPosition=1;

         @Override
         public void onClick(View view) {

             if (position == selectedPosition) {
                 check.setChecked(false);
                 selectedPosition = -1;
             } else {
                 selectedPosition = position;

             }
         }
     });

        //spinner class for send circular
        List<String> categories = new ArrayList<String>();
        categories.add("Jaun Keich");
        categories.add("Stephen");
        categories.add("Caroline");
        categories.add("Daniel");
        categories.add("John");
        categories.add("Michael");
        spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, categories);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        //spinner class for classpinner
        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Class 5C");
        classpinner.add("Class 4C");
        classpinner.add("Class 6C");
        classpinner.add("Class 7A");
        classpinner.add("Class 8B");
        classpinner.add("Class 9C");
        spinner_class = (Spinner) findViewById(R.id.spinner_class);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                this, R.layout.class_spinner_item, classpinner);

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.class_spinner_item);

        spinner_class.setAdapter(spinnerArrayAdapter1);

        spinner_class.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
    upload_linear.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,

                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        // Start the Intent

        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, 7);
            }
});
    }

   public void onActivityResult(int requestCode, int resultCode, Intent data) {

       super.onActivityResult(requestCode, resultCode, data);
       if (resultCode == this.RESULT_CANCELED) {
           return;
       }
       if (requestCode == GALLERY) {
           if (data != null) {
               Uri contentURI = data.getData();
               try {
                   Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), contentURI);
                   String path = saveImage(bitmap);
          Toast.makeText(getApplicationContext(), "Uploaded successfully", Toast.LENGTH_SHORT).show();
                   img_upload.setImageBitmap(bitmap);

               } catch (IOException e) {
                   e.printStackTrace();
                //   Toast.makeText(MainActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
               }
           }

       }

       if (resultCode == RESULT_OK) {
           String PathHolder = data.getData().getPath();
           Toast.makeText(getApplicationContext(), PathHolder, Toast.LENGTH_LONG).show();
       }
   }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


}


























