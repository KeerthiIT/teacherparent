package school.com.teachersclient.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.ResultHomeAdapter;
import school.com.teachersclient.Fragment.ResultFragment;
import school.com.teachersclient.Pojo.ResulthomePojo;
import school.com.teachersclient.R;

public class ResultActivity extends AppCompatActivity {

    private Spinner result_spinner;
    private RecyclerView recyclerView;
    private ResultHomeAdapter mAdapter;
    private List<ResulthomePojo> resList = new ArrayList<>();
    private ImageView noti;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        noti=findViewById(R.id.noti);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),NotificationActivity.class);
                startActivity(intent);
            }
        });
        Fragment fragment = new ResultFragment();
        replaceFragment(fragment);


    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }


/*
    private void Resultdata() {
        ResulthomePojo s=new ResulthomePojo();
       s.setClass_img(String.valueOf(R.drawable.classimg));
       s.setStu_img(String.valueOf(R.drawable.dp));
       s.setClass_name("Class 5C");
       s.setStu_name("Mark Zuckerbak");
       s.setPercent("90%");
       s.setCounts("43 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 6C");
        s.setStu_name("Robert Luiensten");
        s.setPercent("60%");
        s.setCounts("40 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 7C");
        s.setStu_name("Mark Albert Einstein");
        s.setPercent("100%");
        s.setCounts("60 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 9C");
        s.setStu_name("Newton");
        s.setPercent("80%");
        s.setCounts("35 students");
        resList.add(s);

        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 10C");
        s.setStu_name("Robert Williams");
        s.setPercent("98%");
        s.setCounts("13 students");
        resList.add(s);
    }
*/
}
