package school.com.teachersclient.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.AddExamAdapter;
import school.com.teachersclient.Pojo.AddExamPojo;
import school.com.teachersclient.R;

public class AddExamActivity extends Activity {

    private RecyclerView recyclerView;
    private AddExamAdapter mAdapter;
    private List<AddExamPojo> addexamList=new ArrayList<>();
    private Spinner exam_spinner,sec_spinner;
    private RelativeLayout relative_class_spinner,relative_sub_spinner;
    private Spinner class_spinner,sub_spinner;
    private Button add_exm_btn;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_exam);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        List<String> classpinner = new ArrayList<String>();
        classpinner.add("Term Exam I");
        classpinner.add("Term Exam II");
        classpinner.add("Term Exam III");
        classpinner.add("Term Exam IV");

        exam_spinner = (Spinner) findViewById(R.id.exam_spinner);
        add_exm_btn=(Button)findViewById(R.id.add_exm_btn);

        add_exm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Exam Added Successfully",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),CircularActivity.class);
                startActivity(intent);
            }
        });

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.classwork_spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.classwork_spinner_item);

        exam_spinner.setAdapter(spinnerArrayAdapter);

        exam_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });



        List<String> sec = new ArrayList<String>();
        sec.add("5A");
        sec.add("6B");
        sec.add("7A");
        sec.add("8A");
        sec.add("9C");
        sec.add("10A");

        sec_spinner = (Spinner) findViewById(R.id.sec_spinner);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                this, R.layout.classwork_spinner_item, sec);

        spinnerArrayAdapter1.setDropDownViewResource(R.layout.classwork_spinner_item);

        sec_spinner.setAdapter(spinnerArrayAdapter1);

        sec_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });











        recyclerView=(RecyclerView)findViewById(R.id.add_exam_recycle);







        mAdapter = new AddExamAdapter(addexamList);


        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        ExamData();

    }

    private void ExamData() {
        AddExamPojo s=new AddExamPojo();
        s.setDate("10 oct 2018");
        s.setTime("9.30 Am");
        addexamList.add(s);

         s=new AddExamPojo();
        s.setDate("10 nov 2018");
        s.setTime("3.30 Pm");
        addexamList.add(s);


         s=new AddExamPojo();
        s.setDate("10 dec 2018");
        s.setTime("3.30 Pm");
        addexamList.add(s);

         s=new AddExamPojo();
        s.setDate("10 Jan 2019");
        s.setTime("3.30 Pm");
        addexamList.add(s);

        s=new AddExamPojo();
        s.setDate("10 Feb 2019");
        s.setTime("5.30 Pm");
        addexamList.add(s);



        s=new AddExamPojo();
        s.setDate("10 Mar 2019");
        s.setTime("2.30 Pm");
        addexamList.add(s);


        s=new AddExamPojo();
        s.setDate("10 Apr 2019");
        s.setTime("10.30 Am");
        addexamList.add(s);

    }
}
