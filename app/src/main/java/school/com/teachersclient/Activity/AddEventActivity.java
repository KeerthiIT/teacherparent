package school.com.teachersclient.Activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import school.com.teachersclient.Adapter.EventSpinnerAdapter;
import school.com.teachersclient.Pojo.EventSpinnerPojo;
import school.com.teachersclient.R;

public class AddEventActivity extends Activity {

    private LinearLayout lin_date;
    private TextView date,time;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private Spinner class_spinner;
    private Spinner status1;
    private LinearLayout lin_time;
    private int hour,minute;
    private Button event_button;
    private ImageView back;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        date=(TextView)findViewById(R.id.date);
        lin_date=(LinearLayout)findViewById(R.id.lin_date);
        lin_time=(LinearLayout)findViewById(R.id.lin_time);
        event_button=(Button) findViewById(R.id.event_button);
        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        time=(TextView)findViewById(R.id.time);
        String date_n = new SimpleDateFormat("dd MMM  yyyy", Locale.getDefault()).format(new Date());
        date.setText(date_n);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("hh.mm a");
        String strDate = "" + mdformat.format(calendar.getTime());
        display(strDate);

        event_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Events Added Successfully",Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),ExamsActivity.class);
                startActivity(intent);
            }
        });

        lin_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_time) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);



                    TimePickerDialog timePickerDialog = new TimePickerDialog(AddEventActivity.this,
                            new TimePickerDialog.OnTimeSetListener() {

                                public String format;

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    if (hourOfDay == 0) {

                                        hourOfDay += 12;

                                        format = "AM";
                                    }
                                    else if (hourOfDay == 12) {

                                        format = "PM";

                                    }
                                    else if (hourOfDay > 12) {

                                        hourOfDay -= 12;

                                        format = "PM";

                                    }
                                    else {

                                        format = "AM";
                                    }

                                    time.setText(hourOfDay + ":" + minute);
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();

                }
            }
        });


        lin_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == lin_date) {

                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(AddEventActivity.this,
                            new DatePickerDialog.OnDateSetListener() {


                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                    Date dateObj = null;
                                    String a = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                    try {
                                        dateObj = curFormater.parse(a);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SimpleDateFormat formt = new SimpleDateFormat("dd MMM yyyy");

                                    date.setText(formt.format(dateObj));
                                }

                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            }
        });

        List<String> classpinner = new ArrayList<String>();
        classpinner.add("1 Classes");
        classpinner.add("2 Classes");
        classpinner.add("3 Classes");
        classpinner.add("4 Classes");
        classpinner.add("5 Classes");
        class_spinner = (Spinner) findViewById(R.id.class_spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.classwork_spinner_item, classpinner);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.classwork_spinner_item);

        class_spinner.setAdapter(spinnerArrayAdapter);

        class_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });



        ArrayList<EventSpinnerPojo> list=new ArrayList<>();
        list.add(new EventSpinnerPojo("public",R.drawable.eye));
        list.add(new EventSpinnerPojo("private",R.drawable.hand));
        list.add(new EventSpinnerPojo("only me",R.drawable.onlyme));

        Spinner status=(Spinner)findViewById(R.id.status);
        EventSpinnerAdapter adapter=new EventSpinnerAdapter(this,
                R.layout.add_event_spinner,R.id.txt,list);
        status.setAdapter(adapter);

    }

    private void display(String num) {
        TextView time = (TextView) findViewById(R.id.time);
        time.setText(num);
    }
}
