package school.com.teachersclient.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

import school.com.teachersclient.Adapter.MyDividerItemDecoration;
import school.com.teachersclient.Adapter.NotiAdapter;
import school.com.teachersclient.Pojo.NotiPojo;
import school.com.teachersclient.R;

public class NotificationActivity extends Activity {

    private RecyclerView recycle;
    private ArrayList<NotiPojo> notiList=new ArrayList<>();
    private ImageView back;
    private NotiAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        recycle=(RecyclerView)findViewById(R.id.recycle_notifications);
        back=(ImageView)findViewById(R.id.back);
        notiList=new ArrayList<>();
        AnalData();

        back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


       // recycle.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        mAdapter = new NotiAdapter(notiList,this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        recycle.setLayoutManager(mLayoutManager);

        recycle.setItemAnimator(new DefaultItemAnimator());
  recycle.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL,5));
        recycle.setAdapter(mAdapter);
    }

    private void AnalData() {

        NotiPojo s=new NotiPojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setTitle("Circular sent by admin");
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("10 min ago");

        notiList.add(s);


        s=new NotiPojo();
        s.setTitle("Juan sent you a message");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("Hello teacher I would know about my son in classroom and his performance in his studies");
        s.setTime("5 min ago");
        notiList.add(s);


        s=new NotiPojo();
        s.setTitle("Circular sent by Emy werly");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("15 min ago");
        notiList.add(s);



        s=new NotiPojo();
        s.setTitle("Michael posted a feed");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setNotiimg(String.valueOf(R.drawable.orange_circle));
        s.setDesc("The School is organizing an educational field trip to Traffic Park-Goshamal for student of class II");
        s.setTime("2 min ago");
        notiList.add(s);

    }
}
