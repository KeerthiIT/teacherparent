package school.com.teachersclient.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Activity.AddClassWorkActivity;
import school.com.teachersclient.Adapter.SubClassAdapter;
import school.com.teachersclient.Pojo.SubClassPojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubClassFragment extends Fragment {


    private RecyclerView recyclerView;
    private SubClassAdapter mAdapter;
    private List<SubClassPojo> subclassList=new ArrayList<>();

    public SubClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_sub_class,container,false);

        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_sub_class);

        mAdapter = new SubClassAdapter(subclassList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Analdata();


        mAdapter.setOnClickListen(new SubClassAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getActivity(), AddClassWorkActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    private void Analdata() {

        SubClassPojo s=new SubClassPojo();
        s.setTitle("English");
        s.setSubtitle("My mother at sixty two");
        s.setContent("Topic:2.4");
        s.setStatus1("Lorium ipsum dolar sit amet,constructor eclipsing elid, sed do elusmud");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subclassList.add(s);


        s=new SubClassPojo();
        s.setTitle("Science");
        s.setSubtitle("Anatomy of Human Beings");
        s.setContent("Topic:6.4");
        s.setStatus1("Human beings skeletal system and collections of Red Blood cells and White Blood cells");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subclassList.add(s);

        s=new SubClassPojo();
        s.setTitle("Maths");
        s.setSubtitle("Trignomentry Formulae");
        s.setStatus1("Trignometry Formulae of sin,cos,tan theta and describe and solve pythagoras theorem");
        s.setContent("Topic:4");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subclassList.add(s);


        s=new SubClassPojo();
        s.setTitle("Social");
        s.setSubtitle("Indian Constitutional");
        s.setStatus1("Indian constitution of economy and welfare of Indian");
        s.setContent("Topic:8");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subclassList.add(s);



    }

}
