package school.com.teachersclient.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Activity.AddResultActivity;
import school.com.teachersclient.Adapter.ResultStuNameAdapter;
import school.com.teachersclient.Pojo.ResultStuNamePojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */

public class ResultStudentName extends Fragment {


    private RecyclerView recyclerView;
    private ResultStuNameAdapter mAdapter;
    private List<ResultStuNamePojo> resstuList=new ArrayList<>();
    private LinearLayout lin;
    private TextView title;
    private String[] myString;

    public ResultStudentName() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_student_name, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycle_result_stuname);
        lin=(LinearLayout)view.findViewById(R.id.lin);
        recyclerView.setNestedScrollingEnabled(false);



        myString = new String[4];
        for (int i = 0; i < myString.length; i++)
        {
            view.setLayoutParams(
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

            view.getParent();
            Resultdata();

            mAdapter = new ResultStuNameAdapter(resstuList, getActivity());
            recyclerView.setAdapter(mAdapter);
        }





        mAdapter = new ResultStuNameAdapter(resstuList, getActivity());
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnClickListen(new ResultStuNameAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getActivity(), AddResultActivity.class);
                startActivity(intent);
            }
        });


        return view;
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }}


    private void Resultdata() {
        ResultStuNamePojo s=new ResultStuNamePojo();
        s.setSubject("English");
        s.setMarks1("100");
        s.setStmarks("87/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);


        s=new ResultStuNamePojo();

        s.setSubject("Maths");
        s.setMarks1("100");
        s.setStmarks("90/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);

        s=new ResultStuNamePojo();

        s.setSubject("Science");
        s.setMarks1("100");
        s.setStmarks("77/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);

        s=new ResultStuNamePojo();

        s.setSubject("Social");
        s.setMarks1("100");
        s.setStmarks("80/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);


        s=new ResultStuNamePojo();
        s.setSubject("Computer Science");
        s.setMarks1("100");
        s.setStmarks("70/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);



    }

}
