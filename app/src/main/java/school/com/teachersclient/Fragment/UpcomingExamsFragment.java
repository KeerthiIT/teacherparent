package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.UpcomingExamsAdapter;
import school.com.teachersclient.Pojo.UpcomingPojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpcomingExamsFragment extends Fragment {


    private UpcomingExamsAdapter mAdapter;
    private List<UpcomingPojo> upList=new ArrayList<>();
    private RecyclerView recyclerView;

    public UpcomingExamsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_upcoming_exams, container, false);

recyclerView=(RecyclerView)view.findViewById(R.id.recycle_upcoming);

        mAdapter = new UpcomingExamsAdapter(upList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Analdata();
return view;

    }

    private void Analdata() {
        UpcomingPojo s=new UpcomingPojo();
        s.setTitle("Term I");
        s.setDate("July 2018");
        upList.add(s);


        s=new UpcomingPojo();
        s.setTitle("Term II");
        s.setDate("Aug 2018");
        upList.add(s);

        s=new UpcomingPojo();
        s.setTitle("Quarterly");
        s.setDate("Sep 2018");
        upList.add(s);

        s=new UpcomingPojo();
        s.setTitle("Half Yearly");
        s.setDate("Oct 2018");
        upList.add(s);

        s=new UpcomingPojo();
        s.setTitle("Term III");
        s.setDate("Nov 2018");
        upList.add(s);

        s=new UpcomingPojo();
        s.setTitle("Annual");
        s.setDate("Dec 2018");
        upList.add(s);
    }

}
