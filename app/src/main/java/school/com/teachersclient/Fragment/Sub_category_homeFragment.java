package school.com.teachersclient.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Activity.AddHomeWorkActivity;
import school.com.teachersclient.Adapter.SubHomeClassAdapter;
import school.com.teachersclient.Pojo.SubHomePojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Sub_category_homeFragment extends Fragment {


    private RecyclerView recyclerView;
    private SubHomeClassAdapter mAdapter;
    private List<SubHomePojo> subhomeList=new ArrayList<>();

    public Sub_category_homeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sub_category_home, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_sub_category_homefragment);

        mAdapter = new SubHomeClassAdapter(subhomeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnClickListen(new SubHomeClassAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Intent intent=new Intent(getActivity(), AddHomeWorkActivity.class);
                startActivity(intent);
            }
        });
        Analdata();

       return view;

    }

    private void Analdata() {
        SubHomePojo s=new SubHomePojo();
        s.setTitle("English");
        s.setSubtitle("My mother at sixty two");
        s.setContent("Topic:2.4");
        s.setStatus1("Lorium ipsum dolar sit amet,constructor eclipsing elid, sed do elusmud");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subhomeList.add(s);


               s=new SubHomePojo();
        s.setTitle("Science");
        s.setSubtitle("Anatomy of Human Beings");
        s.setContent("Topic:6.4");
        s.setStatus1("Human beings skeletal system and collections of Red Blood cells and White Blood cells");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subhomeList.add(s);


        s=new SubHomePojo();
        s.setTitle("Maths");
        s.setSubtitle("Trignomentry Formulae");
        s.setStatus1("Trignometry Formulae of sin,cos,tan theta and describe and solve pythagoras theorem");
        s.setContent("Topic:4");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subhomeList.add(s);


        s=new SubHomePojo();
        s.setTitle("Social");
        s.setSubtitle("Indian Constitutional");
        s.setStatus1("Indian constitution of economy and welfare of Indian");
        s.setContent("Topic:8");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setSubimg1(String.valueOf(R.drawable.build));
        s.setSubimg2(String.valueOf(R.drawable.build));
        subhomeList.add(s);

    }

}
