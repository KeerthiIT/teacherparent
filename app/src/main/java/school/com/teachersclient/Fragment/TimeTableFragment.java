package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.TimeTableAdapter;
import school.com.teachersclient.Pojo.TimeTablePojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeTableFragment extends Fragment {


    private RecyclerView recyclerView;
    private TimeTableAdapter mAdapter;
    private List<TimeTablePojo> timeList=new ArrayList<>();

    public TimeTableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       View view=inflater.inflate(R.layout.fragment_time_table, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.timetable_recycle);

        mAdapter = new TimeTableAdapter(timeList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Analdata();


        return view;
    }

    private void Analdata() {

        TimeTablePojo s=new TimeTablePojo();
        s.setClasstext("Class 5C");
        s.setSubtext("English");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setTime("9.00-9.30");
        s.setDuration("1st period");
        timeList.add(s);



        s=new TimeTablePojo();
        s.setClasstext("Class 7C");
        s.setSubtext("Maths");
        s.setImg(String.valueOf(R.drawable.user));
        s.setTime("1.00-2.00");
        s.setDuration("3rd period");
        timeList.add(s);


        s=new TimeTablePojo();
        s.setClasstext("Class 8C");
        s.setSubtext("science");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setTime("2.00-3.00");
        s.setDuration("4th period");
        timeList.add(s);


        s=new TimeTablePojo();
        s.setClasstext("Class 9C");
        s.setSubtext("social");
        s.setImg(String.valueOf(R.drawable.user));
        s.setTime("2.00-5.00");
        s.setDuration("5th period");
        timeList.add(s);


        s=new TimeTablePojo();
        s.setClasstext("Class 10C");
        s.setSubtext("social");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setTime("5.00-6.00");
        s.setDuration("6th period");
        timeList.add(s);

    }

}
