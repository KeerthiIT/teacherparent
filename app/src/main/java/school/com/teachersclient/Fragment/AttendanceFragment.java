package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.AttendanceAdapter;
import school.com.teachersclient.Pojo.AttPojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttendanceFragment extends Fragment {


    private RecyclerView recyclerView;
    private AttendanceAdapter mAdapter;
    private List<AttPojo> attList=new ArrayList<>();

    public AttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_attendance, container, false);
        recyclerView=(RecyclerView)view.findViewById(R.id.attendance_fragment_recycle);

        mAdapter = new AttendanceAdapter(attList);


        //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        AttenData();


        mAdapter.OnsetClickListen(new AttendanceAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Fragment fragment=new SubAttendanceFragment();
                replaceFragment(fragment);
            }

            private void replaceFragment(Fragment fragment) {
                String backStateName;
                backStateName = ((Object) fragment).getClass().getName();
                String fragmentTag = backStateName;

                FragmentManager manager = getActivity().getSupportFragmentManager();
                boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

                if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.replace(R.id.frame_attendance, fragment, fragmentTag);
                    ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
                    ft.addToBackStack(backStateName);
                    ft.commit();
                }


            }
        });
        return view;
    }

    private void AttenData() {
        AttPojo s=new AttPojo();
        s.setClass_text("Class 5A");
        s.setImg(String.valueOf(R.drawable.circle));
        s.setImg1(String.valueOf(R.drawable.grl));
        s.setImg2(String.valueOf(R.drawable.woman));
        s.setImg3(String.valueOf(R.drawable.woman));
        s.setPresent_count("30");
        s.setAbsent_count("5");
        s.setS1("01");
        s.setText1("Sick Leave");
        s.setText2("Absent");
        s.setS2("01");
        s.setStu_counts("35 students");
        attList.add(s);

        s=new AttPojo();
        s.setClass_text("Class 5B");
        s.setImg(String.valueOf(R.drawable.circle));
        s.setImg1(String.valueOf(R.drawable.girl));
        s.setImg2(String.valueOf(R.drawable.grl));
        s.setImg3(String.valueOf(R.drawable.grl));
        s.setPresent_count("40");
        s.setAbsent_count("4");
        s.setS1("01");
        s.setText1("Leave");
        s.setText2("Absent");
        s.setS2("02");
        s.setStu_counts("44 students");
        attList.add(s);

        s=new AttPojo();
        s.setClass_text("Class 5A");
        s.setImg(String.valueOf(R.drawable.circle));
        s.setImg1(String.valueOf(R.drawable.grl));
        s.setImg2(String.valueOf(R.drawable.woman));
        s.setImg3(String.valueOf(R.drawable.woman));
        s.setPresent_count("30");
        s.setAbsent_count("5");
        s.setS1("01");
        s.setText1("Sick Leave");
        s.setText2("Absent");
        s.setS2("01");
        s.setStu_counts("35 students");
        attList.add(s);

        s=new AttPojo();
        s.setClass_text("Class 7B");
        s.setImg(String.valueOf(R.drawable.circle));
        s.setImg1(String.valueOf(R.drawable.girl));
        s.setImg2(String.valueOf(R.drawable.woman));
        s.setImg3(String.valueOf(R.drawable.grl));
        s.setPresent_count("40");
        s.setAbsent_count("4");
        s.setS1("01");
        s.setText1("Leave");
        s.setText2("Absent");
        s.setS2("02");
        s.setStu_counts("44 students");
        attList.add(s);

        s=new AttPojo();
        s.setClass_text("Class 5A");
        s.setImg(String.valueOf(R.drawable.circle));
        s.setImg1(String.valueOf(R.drawable.grl));
        s.setImg2(String.valueOf(R.drawable.grl));
        s.setImg3(String.valueOf(R.drawable.woman));
        s.setPresent_count("30");
        s.setAbsent_count("5");
        s.setS1("01");
        s.setText1("Sick Leave");
        s.setText2("Absent");
        s.setS2("01");
        s.setStu_counts("35 students");
        attList.add(s);
    }

}
