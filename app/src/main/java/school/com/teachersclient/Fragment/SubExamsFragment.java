package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.ExamAdapter;
import school.com.teachersclient.Pojo.Exampojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubExamsFragment extends Fragment {


    private RecyclerView recyclerView;
    private ExamAdapter mAdapter;
    private List<Exampojo> examList=new ArrayList<>();

    public SubExamsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_sub_exams, container, false);

       recyclerView=(RecyclerView)view.findViewById(R.id.exam_recycle_fragment);





        mAdapter = new ExamAdapter(examList);
      //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        ExamData();

        mAdapter.setOnClickListen(new ExamAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                Fragment fragment=new Exams3Fragment();
                replaceFragment(fragment);
            }


        });
        return view;



    }

    private void replaceFragment(Fragment fragment) {

            String backStateName;
            backStateName = ((Object) fragment).getClass().getName();
            String fragmentTag = backStateName;

            FragmentManager manager = getActivity().getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

            if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.frame_exam, fragment, fragmentTag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
                ft.addToBackStack(backStateName);
                ft.commit();
            }




    }

    private void ExamData() {
        Exampojo s=new Exampojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setTitle("Ist Term Exam");
        s.setSchool("Burnley High School");
        s.setCounts("Class 1-10");
        s.setDuration("3 Hours");
        s.setStartdate("10 oct 2018");
        s.setEnddate("15 nov 2018");
        examList.add(s);


        s=new Exampojo();
        s.setImg(String.valueOf(R.drawable.user));
        s.setTitle("IInd Term Exam");
        s.setSchool("Burnley High School");
        s.setCounts("Class 11-20");
        s.setDuration("3 Hours");
        s.setStartdate("10 Dec 2018");
        s.setEnddate("01 Jan 2019");
        examList.add(s);

        s=new Exampojo();
        s.setImg(String.valueOf(R.drawable.usergirl));
        s.setTitle("IIIrd Term Exam");
        s.setSchool("Burnley High School");
        s.setCounts("Class 21-30");
        s.setDuration("3 Hours");
        s.setStartdate("10 Jan 2019");
        s.setEnddate("15 Feb 2019");
        examList.add(s);

        s=new Exampojo();
        s.setImg(String.valueOf(R.drawable.dp));
        s.setTitle("Half Yearly Exam");
        s.setSchool("Burnley High School");
        s.setCounts("Class 1-10");
        s.setDuration("3 Hours");
        s.setStartdate("09 Mar 2019");
        s.setEnddate("15 Apr 2019");
        examList.add(s);



    }

}
