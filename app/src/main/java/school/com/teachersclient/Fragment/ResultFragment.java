package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.ResultHomeAdapter;
import school.com.teachersclient.Pojo.ResulthomePojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment {


    private Spinner result_spinner;
    private RecyclerView recyclerView;
    private ResultHomeAdapter mAdapter;
    private List<ResulthomePojo> resList=new ArrayList<>();



    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_result, container, false);

        result_spinner = (Spinner)view.findViewById(R.id.result_spinner);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycle_result_home);

        mAdapter = new ResultHomeAdapter(resList, getActivity());
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        Resultdata();

      mAdapter.setOnClickListen(new ResultHomeAdapter.AddTouchListen() {
    @Override
    public void onTouchClick(int position) {
       Fragment fragment=new ResultClassFragment();
        replaceFragment(fragment);
    }


      });
        List<String> term = new ArrayList<String>();
        term.add("Ist Term Exam");
        term.add("IInd Term Exam");
        term.add("IIIrd Term Exam");
        term.add("Ist Mid Term Exam");
        term.add("IInd Mid Term Exam");
        term.add("IIIrd Mid Term Exam");


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, term);

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        result_spinner.setAdapter(spinnerArrayAdapter);

        result_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });





        return view;
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.result_frame, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }

    private void Resultdata() {
        ResulthomePojo s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 5C");
        s.setStu_name("Mark Zuckerbak");
        s.setPercent("90%");
        s.setCounts("43 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 6C");
        s.setStu_name("Robert Luiensten");
        s.setPercent("60%");
        s.setCounts("40 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 7C");
        s.setStu_name("Mark Albert Einstein");
        s.setPercent("100%");
        s.setCounts("60 students");
        resList.add(s);


        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 9C");
        s.setStu_name("Newton");
        s.setPercent("80%");
        s.setCounts("35 students");
        resList.add(s);

        s=new ResulthomePojo();
        s.setClass_img(String.valueOf(R.drawable.classimg));
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setClass_name("Class 10C");
        s.setStu_name("Robert Williams");
        s.setPercent("98%");
        s.setCounts("13 students");
        resList.add(s);
    }

}
