package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Setting extends Fragment implements CompoundButton.OnCheckedChangeListener  {


    private Switch chat_switch;
    private Button update;

    public Setting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


       View view=inflater.inflate(R.layout.fragment_setting,container,false);
        chat_switch=(Switch)view.findViewById(R.id.chat_switch);
        update=(Button)view.findViewById(R.id.update);


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"updated successfully",Toast.LENGTH_SHORT).show();
            }
        });
  return view;

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }
}
