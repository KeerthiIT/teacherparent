package school.com.teachersclient.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Activity.AddAttendanceActivity;
import school.com.teachersclient.Adapter.SubAttendanceAdapter;
import school.com.teachersclient.Pojo.SubAttPojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubAttendanceFragment extends Fragment {


    private RecyclerView recyclerView;
    private SubAttendanceAdapter mAdapter;
    private List<SubAttPojo> subattList=new ArrayList<>();

    public SubAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sub_attendance, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.sub_attendnce_recycle_fragment);

        mAdapter = new SubAttendanceAdapter(subattList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        mAdapter.setOnClickListen(new SubAttendanceAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
               Intent intent=new Intent(getActivity(), AddAttendanceActivity.class);
               startActivity(intent);
            }
        });
        AttData();
        return view;
    }

    private void AttData() {

        SubAttPojo s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("Alexander");
        s.setPercentage("80%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);


        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Allan");
        s.setPercentage("100%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);



        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("Daniel");
        s.setPercentage("60%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);



        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Charles Mick");
        s.setPercentage("80%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);



        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("Dansi");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);


        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("Jhansi");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);


        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Klinton");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("Caroline");
        s.setPercentage("60%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);


        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Michael");
        s.setPercentage("60%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);

        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("John");
        s.setPercentage("80%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.dp));
        s.setStu_name("Jaun");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.usergirl));
        s.setStu_name("James");
        s.setPercentage("90%");
        s.setStatus(String.valueOf(R.drawable.present));
        subattList.add(s);

        s=new SubAttPojo();
        s.setStu_img(String.valueOf(R.drawable.user));
        s.setStu_name("Lord Williams");
        s.setPercentage("50%");
        s.setStatus(String.valueOf(R.drawable.absent));
        subattList.add(s);


    }

}
