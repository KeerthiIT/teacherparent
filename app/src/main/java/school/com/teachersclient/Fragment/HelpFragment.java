package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment {


    private TextView faq,feedback,privacy,about;
    private ImageView faq_arrow,feedback_arrow,privacy_arrow,about_arrow;

    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view=inflater.inflate(R.layout.fragment_help, container, false);

        faq=(TextView)view.findViewById(R.id.faq);
        feedback=(TextView)view.findViewById(R.id.feedback);
        privacy=(TextView)view.findViewById(R.id.privacy);
        about=(TextView)view.findViewById(R.id.about);

        faq_arrow=(ImageView)view.findViewById(R.id.faq_arrow);
        feedback_arrow=(ImageView)view.findViewById(R.id.feedback_arrow);
        privacy_arrow=(ImageView)view.findViewById(R.id.privacy_arrow);
        about_arrow=(ImageView)view.findViewById(R.id.about_arrow);

        return view;
    }

}
