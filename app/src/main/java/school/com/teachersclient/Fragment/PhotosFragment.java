package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.PhotoGridAdapter;
import school.com.teachersclient.Pojo.PhotoGridPojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhotosFragment extends Fragment {
    private GridView gridView;
    private PhotoGridAdapter gridAdapter;
    private RecyclerView recyclerview;
    private PhotoGridAdapter mAdapter;
    private List<PhotoGridPojo> photoList=new ArrayList<>();


    public PhotosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photos, container, false);
        recyclerview=(RecyclerView)view.findViewById(R.id.recycle_grid);
        mAdapter = new PhotoGridAdapter(photoList,getActivity());


        AnalData();
      /*  RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerview.setLayoutManager(mLayoutManager);*/

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
        recyclerview.setLayoutManager(gridLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.setAdapter(mAdapter);

        return view;
    }

    private void AnalData() {
        PhotoGridPojo s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

        s = new PhotoGridPojo();
        s.setImg(String.valueOf(R.drawable.user));
        photoList.add(s);

    }}