package school.com.teachersclient.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.ClassAdapter;
import school.com.teachersclient.Pojo.ClassPojo;
import school.com.teachersclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentClass extends Fragment {


    private RecyclerView recyclerView;
    private ClassAdapter mAdapter;
    private List<ClassPojo> classList=new ArrayList<>();

    public FragmentClass() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_class, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.recycle_fragment_class);

        mAdapter = new ClassAdapter(classList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        Analdata();

        mAdapter.setOnClickListen(new ClassAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Fragment fragment=new SubClassFragment();
                replaceFragment(fragment);
            }
        });


        return view;

    }

    private void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_classwork, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);//
            ft.addToBackStack(backStateName);
            ft.commit();
        }


    }
    private void Analdata() {
        ClassPojo s=new ClassPojo();
        s.setTitle("Class 5A");
        s.setContent("English");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setImg2(String.valueOf(R.drawable.dp));
        s.setStatus1("My Mother at sixty two");
        s.setStatus2("Topic: 2.4");
        s.setCounts("62%");
        classList.add(s);


        s=new ClassPojo();
        s.setTitle("Class 6C");
        s.setContent("English");
        s.setStatus1("Classwork not updated");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setImg2(String.valueOf(R.drawable.dp));
        s.setCounts("72%");
        classList.add(s);

        s=new ClassPojo();
        s.setTitle("Class 7C");
        s.setContent("English");
        s.setStatus1("Write Shakesphere poem on your claswork note");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setImg2(String.valueOf(R.drawable.dp));
        s.setStatus2("Topic: 6.4");
        s.setCounts("50%");
        classList.add(s);

        s=new ClassPojo();
        s.setTitle("Class 8C");
        s.setContent("English");
        s.setStatus1("Write a Essay about requesting Street light");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setImg2(String.valueOf(R.drawable.dp));
        s.setStatus2("Topic: 7.4");
        s.setCounts("52%");
        classList.add(s);

        s=new ClassPojo();
        s.setTitle("Class 9C");
        s.setContent("English");
        s.setStatus1("Study Vocabulary");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setImg2(String.valueOf(R.drawable.dp));
        s.setStatus2("Topic: 8.4");
        s.setCounts("62%");
        classList.add(s);

        s=new ClassPojo();
        s.setTitle("Class 10C");
        s.setContent("English");
        s.setStatus1("Write prepositions on your classwork note");
        s.setImg(String.valueOf(R.drawable.dp));
        s.setImg2(String.valueOf(R.drawable.dp));
        s.setStatus2("Topic: 9.4");
        s.setCounts("65%");
        classList.add(s);
    }
}
