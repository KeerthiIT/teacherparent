package school.com.teachersclient.Fragment;


import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.github.rtoshiro.view.video.FullscreenVideoLayout;

import java.io.IOException;

import school.com.teachersclient.R;




/**
 * A simple {@link Fragment} subclass.
 */
public class VideosFragment extends Fragment {
    VideoView videoView;
    String video_url = "https://www.youtube.com/watch?v=3GpS1NV9XPQ";
    ProgressDialog pd;
    private VideoView video;
    private MediaController mediacontroller;
    private Uri uri;
    private boolean isContinuously;
    private ProgressDialog pDialog;
    private FullscreenVideoLayout videoLayout;


    public VideosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_videos, container, false);
        videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
        videoLayout.setActivity(getActivity());

        Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        try {
            videoLayout.setVideoURI(videoUri);

        } catch (IOException e) {
            e.printStackTrace();
        }





      /*  pDialog = new ProgressDialog(getActivity());
        // Set progressbar title
        pDialog.setTitle("Android Video Streaming Tutorial");
        // Set progressbar message
        pDialog.setMessage("Buffering...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        // Show progressbar
      pDialog.show();

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                   getActivity());
            mediacontroller.setAnchorView(videoView);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(video_url);
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoView.requestFocus();
        videoView.start();
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                Log.d("log", "onError: ");

                return false;
            }
        });
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
         pDialog.dismiss();
                videoView.start();
            }
        });
*/
        return view;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


}
