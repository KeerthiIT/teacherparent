package school.com.teachersclient.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Adapter.ResultStuNameAdapter;
import school.com.teachersclient.Pojo.ResultStuNamePojo;
import school.com.teachersclient.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResultDetailFragment extends Fragment {


    private RecyclerView recyclerView;
    private List<ResultStuNamePojo> resstuList=new ArrayList<>();
    private ResultStuNameAdapter mAdapter;
    private String[] myString;
    private LinearLayout lin;

    public ResultDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.result_item, container, false);

        recyclerView=view.findViewById(R.id.recycle_result_stuname);


        mAdapter = new ResultStuNameAdapter(resstuList, getActivity());
        //   recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);


        Resultdata();
        LayoutInflater inflator = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < myString.length; i++) {
            // inflate child
            View item = inflator.inflate(R.layout.result_item, null);
            lin = (LinearLayout) item.findViewById(R.id.lin);
            lin.addView(item);
        }



        return view;
    }
    private void Resultdata() {
        ResultStuNamePojo s=new ResultStuNamePojo();
        s.setSubject("English");
        s.setMarks1("100");
        s.setStmarks("87/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);


        s=new ResultStuNamePojo();

        s.setSubject("Maths");
        s.setMarks1("100");
        s.setStmarks("90/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);

        s=new ResultStuNamePojo();

        s.setSubject("Science");
        s.setMarks1("100");
        s.setStmarks("77/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);

        s=new ResultStuNamePojo();

        s.setSubject("Social");
        s.setMarks1("100");
        s.setStmarks("80/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);


        s=new ResultStuNamePojo();
        s.setSubject("Computer Science");
        s.setMarks1("100");
        s.setStmarks("70/");
        s.setImg(String.valueOf(R.drawable.circle));
        resstuList.add(s);



    }


}
