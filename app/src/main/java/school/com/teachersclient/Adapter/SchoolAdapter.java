package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.SchoolPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 10/15/2018.
 */


public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.MyViewHolder> {

    private List<SchoolPojo> schList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;


    public SchoolAdapter(List<SchoolPojo> schList) {

        this.schList=schList;
    }

    /*public ClassAdapter(List<ClassPojo> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin;
        private  TextView desc,content,stu_name,stu_post;
        private  ImageView dp;

        public MyViewHolder(View view) {
            super(view);
            dp = (ImageView) view.findViewById(R.id.dp);
            desc=(TextView)view.findViewById(R.id.desc);
            content = (TextView) view.findViewById(R.id.content);
            stu_name=(TextView)view.findViewById(R.id.stu_name);
            stu_post=(TextView)view.findViewById(R.id.stu_post);
            lin=(LinearLayout)view.findViewById(R.id.lin);



        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.school_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SchoolPojo movie = schList.get(position);
        holder.stu_name.setText(movie.getStu_name());
        holder.content.setText(movie.getContent());
        holder.dp.setImageResource(Integer.parseInt(movie.getDp()));
        holder.stu_post.setText(movie.getStu_post());
        holder.desc.setText(movie.getDesc());
        holder.content.setText(movie.getContent());

        holder.lin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return schList.size();
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}