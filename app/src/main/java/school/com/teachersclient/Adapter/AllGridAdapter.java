package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import com.github.rtoshiro.view.video.FullscreenVideoLayout;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.AllGridPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/19/2018.
 */

public class AllGridAdapter extends RecyclerView.Adapter<AllGridAdapter.MyViewHolder> {

    private  FullscreenVideoLayout videoLayout;
    private List<AllGridPojo> gridList=new ArrayList<>();
    private Context context;
//    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        public VideoView v;


        public MyViewHolder(View view) {
            super(view);
            img=(ImageView)view.findViewById(R.id.user);
          /*  videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
            videoLayout.setActivity((Activity) context);
*/
        }
    }


    public AllGridAdapter(List<AllGridPojo> gridList,Context context) {
        this.gridList = gridList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AllGridPojo movie = gridList.get(position);

        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
      /*  Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        holder.v.setVideoURI(videoUri);*/

    }

    @Override
    public int getItemCount() {
        return gridList.size();
    }
}