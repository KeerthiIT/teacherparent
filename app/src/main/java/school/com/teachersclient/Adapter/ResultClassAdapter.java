package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.ResultClassPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 10/1/2018.
 */


public class ResultClassAdapter extends RecyclerView.Adapter<ResultClassAdapter.MyViewHolder> {

    private List<ResultClassPojo> resclassList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView name,counts,date;
        public TextView content;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            counts = (TextView) view.findViewById(R.id.counts);
            img=(ImageView)view.findViewById(R.id.profile);
            lin=(LinearLayout)view.findViewById(R.id.lin);

        }
    }


    public ResultClassAdapter(List<ResultClassPojo> resclassList,Context context) {
        this.resclassList = resclassList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_class_members, parent, false);

        return new MyViewHolder(itemView);
    }
public void setOnclickListen(AddTouchListen addTouchListen)
{
   this.addTouchListen= addTouchListen;
}
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ResultClassPojo movie = resclassList.get(position);
        holder.name.setText(movie.getName());
        holder.counts.setText(movie.getCounts());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return resclassList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);
    }
}