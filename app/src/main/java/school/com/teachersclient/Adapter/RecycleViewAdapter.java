package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import school.com.teachersclient.Pojo.GridPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/18/2018.
 */
public class RecycleViewAdapter extends RecyclerView.Adapter {

    ArrayList mValues;
    Context mContext;

    protected ItemListener mListener;
    private ViewHolder Vholder;

    public RecycleViewAdapter(Context context, ArrayList values, ItemListener itemListener) {

        mValues = values;
        mContext = context;
        mListener=itemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView textView,textView1;
        public GridView imageView;

        GridPojo item;

        public ViewHolder(View v) {

            super(v);

            v.setOnClickListener(this);

        //    imageView = (GridView) v.findViewById(R.id.dp);


        }

        public void setData(GridPojo item) {
            this.item = item;


            imageView.setColumnWidth(Integer.parseInt(item.dp));

        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    @Override
    public RecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.grid_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Vholder.setData((GridPojo) mValues.get(position));
    }



    @Override
    public int getItemCount() {

        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(GridPojo item);
    }
}