package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.ResultStuNamePojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 10/4/2018.
 */


public class ResultStuNameAdapter extends RecyclerView.Adapter<ResultStuNameAdapter.MyViewHolder> {

    private final LayoutInflater inflater;
    private List<ResultStuNamePojo> restuList=new ArrayList<>();
    private Context context;
    private final int VIEW_TYPE_TEXTVIEW = 0;
    private final int VIEW_TYPE_ITEM_1 = 1;
    private final int VIEW_TYPE_ITEM_2 = 2;
    public AddTouchListen addTouchListen;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin;
        private  TextView title,subject,total,stmarks;
        private  ImageView img;

        public MyViewHolder(View view) {
            super(view);

         subject=(TextView)view.findViewById(R.id.subject);
         img=(ImageView)view.findViewById(R.id.img);
         total=(TextView)view.findViewById(R.id.marks1);
         stmarks=(TextView)view.findViewById(R.id.stmarks);
         lin=(LinearLayout)view.findViewById(R.id.lin);

        }
    }


    public ResultStuNameAdapter(List<ResultStuNamePojo> restuList,Context context) {
        this.restuList = restuList;
        this.context =context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_stuname_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ResultStuNamePojo movie = restuList.get(position);


        holder.subject.setText(movie.getSubject());
        holder.total.setText(movie.getMarks1());
      holder.stmarks.setText(movie.getStmarks());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));


        holder.lin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return restuList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}