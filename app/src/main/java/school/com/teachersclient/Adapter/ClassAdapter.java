package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.ClassPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/20/2018.
 */

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.MyViewHolder> {

    private List<ClassPojo> classList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;
    private ProgressBar prg1;
    private ProgressBar Prg1;
    private ProgressBar prg2;

    public ClassAdapter(List<ClassPojo> classList) {

        this.classList=classList;
    }

    /*public ClassAdapter(List<ClassPojo> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

       // private ProgressBar prg1,prg2;
        private  TextView counts;
        private  ImageView img,img2;
        private  TextView title,content,status1,status2;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title1);
            content = (TextView) view.findViewById(R.id.content1);
            img=(ImageView)view.findViewById(R.id.img);
            img2=(ImageView)view.findViewById(R.id.img2);
            status1=(TextView)view.findViewById(R.id.status1);
            status2=(TextView)view.findViewById(R.id.status2);
            counts=(TextView)view.findViewById(R.id.counts);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.class_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ClassPojo movie = classList.get(position);
        holder.title.setText(movie.getTitle());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.img2.setImageResource(Integer.parseInt(movie.getImg2()));
        holder.status2.setText(movie.getStatus2());
        holder.status1.setText(movie.getStatus1());
        holder.counts.setText(movie.getCounts());


        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}