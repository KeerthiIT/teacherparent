package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.SubHomePojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/27/2018.
 */


public class SubHomeClassAdapter extends RecyclerView.Adapter<SubHomeClassAdapter.MyViewHolder> {

    private List<SubHomePojo> subhomeList=new ArrayList<>();
    private Context context;
    public AddTouchListen addTouchListen;
    public SubHomeClassAdapter(List<SubHomePojo> subhomeList) {

        this.subhomeList=subhomeList;
    }

    /*public ClassAdapter(List<ClassPojo> classList) {
        this.classList = classList;

    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView counts;
        private ImageView img,subimg1,subimg2;
        private  TextView title,content,status,subtitle;
        public LinearLayout lin;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle=(TextView)view.findViewById(R.id.subtitle);
            content = (TextView) view.findViewById(R.id.content);
            img=(ImageView)view.findViewById(R.id.img);
            subimg1=(ImageView)view.findViewById(R.id.subimg1);
            status=(TextView)view.findViewById(R.id.status);
            subimg2=(ImageView) view.findViewById(R.id.subimg2);
            lin=(LinearLayout)view.findViewById(R.id.lin);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_home_class_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SubHomePojo movie = subhomeList.get(position);
        holder.title.setText(movie.getTitle());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.subimg1.setImageResource(Integer.parseInt(movie.getSubimg1()));
        holder.subimg2.setImageResource(Integer.parseInt(movie.getSubimg2()));
        holder.subtitle.setText(movie.getSubtitle());
        holder.status.setText(movie.getStatus1());

        holder.lin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if(addTouchListen!=null)
                {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return subhomeList.size();
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}