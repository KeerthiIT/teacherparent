package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.ResulthomePojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 10/1/2018.
 */

public class ResultHomeAdapter extends RecyclerView.Adapter<ResultHomeAdapter.MyViewHolder> {

    private List<ResulthomePojo> resList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private  LinearLayout lin;
        private  ImageView class_img,stu_img;
        private  TextView class_name,stu_name,percent,counts;

        public MyViewHolder(View view) {
            super(view);
            class_name = (TextView) view.findViewById(R.id.classname);
            stu_name = (TextView) view.findViewById(R.id.stuname);
            class_img=(ImageView)view.findViewById(R.id.class_img);
            stu_img=(ImageView)view.findViewById(R.id.stu_img);
            percent=(TextView)view.findViewById(R.id.percent);
            counts=(TextView)view.findViewById(R.id.count);
            lin=(LinearLayout)view.findViewById(R.id.lin);
        }
    }


    public ResultHomeAdapter(List<ResulthomePojo> resList,Context context) {
        this.resList = resList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.result_home_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ResulthomePojo movie = resList.get(position);
        holder.class_name.setText(movie.getClass_name());
        holder.stu_name.setText(movie.getStu_name());
        holder.percent.setText(movie.getPercent());
        holder.counts.setText(movie.getCounts());
        holder.class_img.setImageResource(Integer.parseInt(movie.getClass_img()));
        holder.stu_img.setImageResource(Integer.parseInt(movie.getStu_img()));

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return resList.size();
    }

    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}