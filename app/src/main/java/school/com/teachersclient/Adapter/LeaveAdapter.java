package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.LeavePojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/11/2018.
 */
public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.MyViewHolder> {

    private List<LeavePojo> leaveList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView desc,subdesc,date;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            subtitle = (TextView) view.findViewById(R.id.subtitle);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.imgim);
           desc=(TextView)view.findViewById(R.id.desc);
           subdesc=(TextView)view.findViewById(R.id.subdesc);
           date=(TextView)view.findViewById(R.id.date);
           time=(TextView)view.findViewById(R.id.time);
           lin=view.findViewById(R.id.lin);
        }
    }


    public LeaveAdapter(List<LeavePojo> leaveList,Context context) {
        this.leaveList = leaveList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        LeavePojo movie = leaveList.get(position);
        holder.title.setText(movie.getTitle());
        holder.subtitle.setText(movie.getSubtitle());
        holder.desc.setText(movie.getDesc());
        holder.subdesc.setText(movie.getSubdesc());
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return leaveList.size();
    }

    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}