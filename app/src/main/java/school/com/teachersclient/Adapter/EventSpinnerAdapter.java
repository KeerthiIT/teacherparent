package school.com.teachersclient.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import school.com.teachersclient.Pojo.EventSpinnerPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 10/5/2018.
 */


public class EventSpinnerAdapter extends ArrayAdapter<EventSpinnerPojo> {
    int groupid;
    Activity context;
    ArrayList<EventSpinnerPojo> list;
    LayoutInflater inflater;
    public EventSpinnerAdapter(Activity context, int groupid, int id, ArrayList<EventSpinnerPojo>
            list){
        super(context,id,list);
        this.list=list;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid=groupid;
    }

    public View getView(int position, View convertView, ViewGroup parent ){
        View itemView=inflater.inflate(groupid,parent,false);
        ImageView imageView=(ImageView)itemView.findViewById(R.id.img);
        imageView.setImageResource(list.get(position).getImageId());
        TextView textView=(TextView)itemView.findViewById(R.id.txt);
        textView.setText(list.get(position).getText());
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent){
        return getView(position,convertView,parent);

    }
}
