package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.EventsPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/12/2018.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {

    private List<EventsPojo> eventList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        ImageView img;
        public TextView desc,date,time;
        public TextView content;
        public LinearLayout lin;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            content=(TextView)view.findViewById(R.id.content);
            img=(ImageView) view.findViewById(R.id.img);
            date=(TextView)view.findViewById(R.id.date);
            time=(TextView)view.findViewById(R.id.time);
            lin=view.findViewById(R.id.lin);

        }
    }


    public EventsAdapter(List<EventsPojo> eventList,Context context) {
        this.eventList = eventList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        EventsPojo movie = eventList.get(position);
        holder.title.setText(movie.getTitle());
        holder.desc.setText(movie.getDesc());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
        holder.date.setText(movie.getDate());
        holder.time.setText(movie.getTime());

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }
    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
}