package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.UpcomingPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/14/2018.
 */

public class PostExamsAdapter extends RecyclerView.Adapter<PostExamsAdapter.MyViewHolder> {

    private List<UpcomingPojo> poList=new ArrayList<>();
    private Context context;

    public PostExamsAdapter(List<UpcomingPojo> upList) {
        this.poList = upList;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        UpcomingPojo movie = poList.get(position);
        holder.title.setText(movie.getTitle());
        holder.date.setText(movie.getDate());

    }

    @Override
    public int getItemCount() {
        return poList.size();
    }
}