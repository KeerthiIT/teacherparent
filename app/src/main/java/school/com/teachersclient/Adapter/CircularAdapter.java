
        package school.com.teachersclient.Adapter;

        import android.content.Context;
        import android.graphics.Color;
        import android.support.v7.widget.CardView;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.MotionEvent;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.PopupWindow;
        import android.widget.TextView;

        import java.util.ArrayList;
        import java.util.List;
        import java.util.zip.Inflater;

        import school.com.teachersclient.Pojo.CircularPojo;
        import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/12/2018.
 */

public  class CircularAdapter extends RecyclerView.Adapter<CircularAdapter.MyViewHolder> {

    private  LinearLayout top;
    private  ImageView menuu;
    private List<CircularPojo> circularlist=new ArrayList<>();
    private Context context;
    private int[] android_versionnames;
    private int row_index;
    private int focusedItem = 0;
    private int position;
    private View itemView;
    private LinearLayout lin;
    private MotionEvent event;
    private View v;
    private View view;
    private Context con;
    private PopupWindow pwindo;
    private AddTouchListen addTouchListen;
    public static final int MAX_LINES =2;
    public TextView desc;
    public String text = "Welcome to Android Master, This is online tutorials for Android Development from beginners and all.We offer collections of  different Android tutorials which are commonly used in development now days.";
    private Inflater inflater;


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private  LinearLayout lin,top;
        private  View view_border;
        private  CardView mCardViewBottom;
        public TextView title;
        ImageView img;

        public TextView content;
        public LinearLayout lin1;
        private TextView tv1;
        public ImageView menuu;
        public TextView desc;



        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            desc = (TextView) view.findViewById(R.id.desc);
            content=(TextView)view.findViewById(R.id.content);
            lin1=(LinearLayout)view.findViewById(R.id.lin1);
            lin=(LinearLayout)view.findViewById(R.id.lin);
            top=(LinearLayout)view.findViewById(R.id.top);
            menuu=(ImageView)view.findViewById(R.id.menu);
            view_border=(View)view.findViewById(R.id.view_border);
            mCardViewBottom = (CardView)view.findViewById(R.id.card_view_bottom);
            //            mCardViewBottom.setCardBackgroundColor(Color.parseColor("#f2faff"));
            mCardViewBottom.setCardBackgroundColor(Color.parseColor("#ffffff"));

        }


       /* public void bind(CircularPojo movie) {

            boolean expanded = movie.isExpanded();

            subItem.setVisibility(expanded ? View.VISIBLE : View.GONE);

            title.setText(movie.getTitle());
            genre.setText("Genre: " + movie.getGenre());
            year.setText("Year: " + movie.getYear());
        }*/
    }
  /*  public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new MyViewHolder(inflater.inflate(mLayoutResID, viewGroup, false));
    }

*/
    public CircularAdapter(List<CircularPojo> circularlist,Context context) {
        this.circularlist = circularlist;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.circular_item, parent, false);

        return new MyViewHolder(itemView);
    }


    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        // holder.tv1.setText(android_versionnames[position]);
        CircularPojo movie = circularlist.get(position);
        holder.title.setText(movie.getTitle());
        holder.desc.setText(movie.getDesc());
        holder.content.setText(movie.getContent());

        holder.lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(addTouchListen!=null)
                {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

       // holder.bind(movie);

       /* holder.itemView.setOnClickListener(v -> {
            boolean expanded = movie.isExpanded();
            movie.setExpanded(!expanded);
            notifyItemChanged(position);
        });
*/
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        //   menuu=(ImageView)v.findViewById(R.id.menu);
        holder.menuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTouchListen.onTouchClick(position);
            }
        });


        if(position==0)
        {
            holder.menuu.setVisibility(View.VISIBLE);

        }
        if (position==1)
        {
            holder.view_border.setVisibility(View.VISIBLE);

            // lin.setBackgroundColor(Color.parseColor("#ffffff"));

        }


    }


    @Override
    public int getItemCount() {
        return circularlist.size();
    }
    public interface AddTouchListen {

        public void onTouchClick(int position);
    }
}
