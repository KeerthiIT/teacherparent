package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.MessagePojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/11/2018.
 */

public class Messageadapter extends RecyclerView.Adapter<Messageadapter.MyViewHolder> {

    private List<MessagePojo> messagelist=new ArrayList<>();
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, subtitle, time;
        ImageView img;
        LinearLayout lin;
        public TextView desc,subdesc,date;
        public TextView content;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            time = (TextView) view.findViewById(R.id.time);
            img=(ImageView)view.findViewById(R.id.img);
            content=(TextView)view.findViewById(R.id.content);
        }
    }


    public Messageadapter(List<MessagePojo> messagelist,Context context) {
        this.messagelist = messagelist;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.msg_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessagePojo movie = messagelist.get(position);
        holder.title.setText(movie.getTitle());
        holder.time.setText(movie.getTime());
        holder.content.setText(movie.getContent());
        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
    }

    @Override
    public int getItemCount() {
        return messagelist.size();
    }
}