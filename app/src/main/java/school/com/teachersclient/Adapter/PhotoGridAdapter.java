package school.com.teachersclient.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import school.com.teachersclient.Pojo.PhotoGridPojo;
import school.com.teachersclient.R;

/**
 * Created by keerthana on 9/19/2018.
 */

public class PhotoGridAdapter extends RecyclerView.Adapter<PhotoGridAdapter.MyViewHolder> {

    private List<PhotoGridPojo> photoList=new ArrayList<>();
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img;


        public MyViewHolder(View view) {
            super(view);
            img=(ImageView)view.findViewById(R.id.user);

        }
    }


    public PhotoGridAdapter(List<PhotoGridPojo> photoList,Context context) {
        this.photoList = photoList;
        this.context =context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photo_view_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PhotoGridPojo movie = photoList.get(position);

        holder.img.setImageResource(Integer.parseInt(movie.getImg()));
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }
}