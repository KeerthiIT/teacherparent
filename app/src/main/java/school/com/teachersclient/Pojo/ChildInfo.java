package school.com.teachersclient.Pojo;

/**
 * Created by keerthana on 9/20/2018.
 */

public class ChildInfo {

    private String sequence = "";
    private String name = "";

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}