package school.com.teachersclient.Pojo;

import java.util.List;

/**
 * Created by keerthana on 10/10/2018.
 */

public class ExCirPojo {
    private String subtitle;
    private boolean expanded;

    public ExCirPojo(String s, List<CircularPojo> artists) {
    }


    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public boolean isExpanded() {
        return expanded;
    }
}
