package school.com.teachersclient.Pojo;

/**
 * Created by keerthana on 10/5/2018.
 */

public class AddResultPojo {

    private String img, name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
