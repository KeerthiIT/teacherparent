package school.com.teachersclient.Pojo;

/**
 * Created by keerthana on 10/5/2018.
 */

public class EventSpinnerPojo {
    String text;
    Integer imageId;
    public EventSpinnerPojo(String text, Integer imageId){
        this.text=text;
        this.imageId=imageId;
    }

    public String getText(){
        return text;
    }

    public Integer getImageId(){
        return imageId;
    }
}
