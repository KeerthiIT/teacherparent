package school.com.teachersclient.Pojo;

/**
 * Created by keerthana on 10/15/2018.
 */

public class SchoolPojo {

    private String  dp,desc,stu_name,stu_post,content;

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStu_name() {
        return stu_name;
    }

    public void setStu_name(String stu_name) {
        this.stu_name = stu_name;
    }

    public String getStu_post() {
        return stu_post;
    }

    public void setStu_post(String stu_post) {
        this.stu_post = stu_post;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
